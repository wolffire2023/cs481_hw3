import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}
class MyApp extends StatefulWidget{
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  bool _imageSelect = true;

  Widget build(BuildContext context) {
    Color color = Theme
        .of(context)
        .primaryColor;
    Widget textSection1 = Container(
      padding: const EdgeInsets.only(left: 20.0),
      child: Row(

        children: [
          _buildButtonColumn(color, Icons.filter_1),

          Text('''  This is based off the idea of 
  making a flying type eeveelution'''
          ),

        ],
      ),

    );
    Widget textSection2 = Container(
      padding: const EdgeInsets.only(left: 20.0, bottom: 20.0),
      child: Row(

        children: [
          _buildButtonColumn(color, Icons.filter_2),

          Text('''  This is based on a bizzare gen 
   1 model of registeel. Otherwise 
   mostly an inside joke.'''
          ),

        ],
      ),

    );
    Widget image1 = Container(
        child: (
            Image.asset('assets/images/Skyeevee.png',
              fit: BoxFit.cover,))
    );
    Widget image2 = Container(
        child: (
            Image.asset('assets/images/melted.png',
              fit: BoxFit.cover,)

        )
    );
    Widget MyButton = Container(
        padding: EdgeInsets.all(0),
        child: Row(
        children: [

          IconButton(
            icon: (_imageSelect ? Icon(Icons.star_border) : Icon(Icons.star)),
            color: Colors.blue,
            onPressed: () {
                _toggle();
                }
                ),
          Text('Press star to toggle images', textAlign: TextAlign.center,),
        ]
        )

        );



    Widget textSectionDescription = Container(
      padding: const EdgeInsets.only(
          top: 10.0, left: 10.0, right: 10.0, bottom: 20.0),
      child: Text(
        'Hello my name is Tyler Young. I am currently a CS major at CSUSM with'
            'a minor in arts and technology I have also done a bunch of art projects'
            'on my own over the past year and half to 2 years and I wanted to make a'
            'collection of a few of them in a conveniently stored place. '
        ,
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.w500),

      ),

    );

    int imageSelected = 1;
    return MaterialApp(
      title: 'My Art Collection',
      home: Scaffold(
          appBar: AppBar(
            title: Text('My Art Collection'),
          ), body: ListView(
        children: [
          textSectionDescription,
          (_imageSelect ? image1 : image2),
          (_imageSelect ? textSection1 : textSection2 ),
          MyButton,

        ],
      )
        //
      ),
    );


  }



  Column _buildButtonColumn(Color color, IconData icon)
  {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(icon,color: color),
      ],
    );


  }
void _toggle() {
  setState(() {
    if(_imageSelect) {
      _imageSelect = false;
    }else {
      _imageSelect = true;
    }
  });


}
}
